import React, { useState, useEffect } from 'react';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';

import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from "./theme";
import GlobalTheme from "./globals";

import QuemSomos from './screens/QuemSomos'
import Home from './screens/Home'
import SinaisDaLiberdade from './screens/SinaisDaLiberdade'
import JornalistaInclusivo from './screens/JornalistaInclusivo'
import Enem from './screens/Enem'
import LinksUteis from './screens/LinksUteis'
import Navbar from './components/NavBar';
import Footer from './components/Footer';

function App() {
  const [theme, setTheme] = useState("light");

  const toggleTheme = () => {
    if (theme === "light") {
      window.localStorage.setItem("theme", "dark");
      setTheme("dark");
    } else {
      window.localStorage.setItem("theme", "light");
      setTheme("light");
    }
  };

  useEffect(() => {
    const localTheme = window.localStorage.getItem("theme");
    localTheme && setTheme(localTheme);
  }, []);


  const [counter, setCounter] = useState(112.5)

  const handleClick1 = () => {
    setCounter(counter - 10)
    document.body.style.fontSize = `${counter}%`
  }

  const handleClick2 = () => {
    setCounter(112.5)
    document.body.style.fontSize = `${counter}%`
  }

  const handleClick3 = () => {
    setCounter(counter + 10)
    document.body.style.fontSize = `${counter}%`
  }


  useEffect(function() {
    let barra = document.querySelector('.top-acessibilityActions')
    function posicaoScroll() {
      if (window.scrollY > 40 && barra) {
       barra.style.position = 'fixed'
       barra.style.right = '0'

      } else 
      barra.style.position = 'initial'
    }

    window.addEventListener('scroll', posicaoScroll)
  },[]);
  
  return (
    <ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
      <GlobalTheme />
     
      <a className="skip-to-content" href="#content">Pular para conteúdo</a>
      <section className="top-acessibilityActions">
        <div className="container d-flex justify-content-end buttonActions">
          
          <div className="buttonActions-secondDiv">
            <button onClick={handleClick1} className="btn btn-secondary btn-sm">A-</button>
            <button onClick={handleClick2} style={{marginLeft: '10px', marginRight: '10px'}} className="btn btn-secondary btn-sm">A</button>
            <button onClick={handleClick3} style={{marginRight: '10px'}}className="btn btn-secondary btn-sm">A+</button>
          
            <button id="click" className="contrast-none" onClick={toggleTheme}>
              <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                <mask id="mask0_139_20" maskUnits="userSpaceOnUse" x="0" y="0" width="23"
                  height="23">
                  <circle cx="11.5" cy="11.5" r="11.5" fill="white" />
                </mask>
                <g mask="url(#mask0_139_20)">
                  <rect x="-12" width="23" height="23" fill="white" />
                  <circle cx="10.5" cy="11.5" r="11" stroke="white" fill="none" />
                </g>
              </svg>
            </button>
          </div>
        </div>
      </section>
      <Router>
        <Navbar />
        <Routes>

          <Route path='/' element={<Home />} />
          <Route path='quemSomos' element={<QuemSomos />} />
          <Route path='sinaisDaLiberdade' element={<SinaisDaLiberdade />} />
          <Route path='jornalistaInclusivo' element={<JornalistaInclusivo />} />
          <Route path='enem' element={<Enem />} />
          <Route path='linksUteis' element={<LinksUteis />} />
        </Routes>
        <Footer />
      </Router>
    </ThemeProvider>
  );
}

export default App;
