import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

body {
  background: ${({ theme }) => theme.body};
  color: ${({ theme }) => theme.text};
  margin: 0;
  padding: 0;
  font-family: sans-serif;
  transition: all 0.25s linear;
  fontSize: ${({ theme }) => theme.tamanho}
}
h1, h2,h3 {
  color: ${({ theme }) => theme.headingTags};
}
  
header {
  background-color: ${({ theme }) => theme.header};
  display: flex;
  align-items: center;
  height: 100px;
}
#responsiveMenu {
  background-color: ${({ theme }) => theme.header}
}
  
.container-description {
  background-color: ${({ theme }) => theme.body};
}
  
footer {
  background-color: ${({ theme }) => theme.header};
}
.warning-content {
  background-color: ${({ theme }) => theme.topBar};  
}
  
a:not(.contrast-none) {
  color: ${({ theme }) => theme.link};
  text-decoration: ${({ theme }) => theme.teste};
}
a:not(.js-tabMenu li a) {
  color: ${({ theme }) => theme.linkGeral};
}

  
.labelOpenMenu>svg {
  fill: #FFFFFF;
}
  
svg rect {
  fill: ${({ theme }) => theme.svg};
}
  
circle {
  stroke: ${({ theme }) => theme.svg};
}
  
.top-acessibilityActions {
  background-color: ${({ theme }) => theme.topBar};
}

.#responsiveMenu {
  background-color: ${({ theme }) => theme.body};
}
  
.contrast-none {
  color: ${({ theme }) => theme.text};
}
`;
