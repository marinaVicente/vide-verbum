import './../assets/styles/geral.css'

function Footer() {
  return (
    <>
      <footer className='d-flex justify-content-center align-items-center' style={{height: '100px', marginTop: '60px'}}>
        <span style={{color: '#FFFFFF'}}>&copy; Araçatuba 2022</span>
      </footer>
    </>

  )
}

export default Footer