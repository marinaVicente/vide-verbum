import { Link } from 'react-router-dom'
import Logo from './../assets/img/logo.svg'

function Navbar() {

  return (
    <>
      <header>
        <div className="container d-flex align-items-center container-logo">
          <label for="menuNoJs" className="responsiveButton d-lg-none" id="abrirMenu" >
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30px" height="30px">
              <path
                d="M 3 7 A 1.0001 1.0001 0 1 0 3 9 L 27 9 A 1.0001 1.0001 0 1 0 27 7 L 3 7 z M 3 14 A 1.0001 1.0001 0 1 0 3 16 L 27 16 A 1.0001 1.0001 0 1 0 27 14 L 3 14 z M 3 21 A 1.0001 1.0001 0 1 0 3 23 L 27 23 A 1.0001 1.0001 0 1 0 27 21 L 3 21 z" />
            </svg>
          </label>
          <Link to="/">
            <img src={Logo} alt="Logo Vide Verbum. Site de notícias e acessibilidade" width="250" />
          </Link>
          <input type="checkbox" name="s" value="s" className="d-none" id="menuNoJs"></input>
          <label id="darkBackground" for="menuNoJs" className="d-lg-none"></label>
          <nav id="responsiveMenu">
            <ul className='js-tabMenu'>
              <li>
                <label for="menuNoJs" className='w-100'>
                  <Link to="/">Home</Link>
                </label>
              </li>
              <li>
                <label for="menuNoJs">
                  <Link to="/quemSomos">Quem Somos</Link>
                </label>
              </li>
              <li>
                <label for="menuNoJs">
                  <Link to="/sinaisDaLiberdade">Sinais da Liberdade</Link>
                </label>
              </li>
              <li>
                <label for="menuNoJs">
                  <Link to="/jornalistaInclusivo">Jornalista Inclusivo</Link>
                </label>
              </li>
              <li>
                <label for="menuNoJs">
                  <Link to="/enem">Enem 2022</Link>
                </label>
              </li>
              <li>
                <label for="menuNoJs" className='w-100'>
                  <Link to="/linksUteis">Links Úteis</Link>
                </label>
              </li>
            </ul>
          </nav>
        </div>
      </header>

    </>

  )
}

export default Navbar