import FotoEntrevista from './../assets/img/jornalista-inclusivo/entrevista-com-rafael.jpg'
import DescricaoJornalistaInclusivo from './../assets/mp3/audio-jornalista-inclusivo.mp3'

function JornalistaInclusivo() {
  return (
    <div>
      <main id="content">
        <div className="container js-tabcontent">

          <section>
            <div className="row justify-content-center pt-5 mb-4">
              <div className="col-12 d-flex justify-content-center">
                <audio src={DescricaoJornalistaInclusivo} controls />
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-12 col-md-10">
                <h1 className="text-center mb-0"> Jornalista Inclusivo</h1>
                <p style={{fontSize: '20px', textAlign: 'center', marginBottom: '60px'}} className="not-color">Transcrição da entrevista com Rafael Ferraz</p>
                <div className="d-flex justify-content-center">
                  <img src={FotoEntrevista} width="70%" alt="Foto da entrevista com Rafael Ferraz realizada por video chamada. 
                            Na imagem de cima para baixo temos Rafael Ferraz, um homem maduro branco, com cabelo escuro rente a cabeça, ele usa óculos e blusa cinza.
                            Isabela, jovem asiática de cabelos lisos e escuros usando uma blusa de mangas compridas marrom e óculos, ela usa fone de ouvido preto. Gabriella, jovem branca de cabelos longos e claros, ela usa camisa social azul claro e headphone rosa." className="mb-5" />
                </div>
                <div id="lipsum">
                  <p>
                    Você sabe o que é inclusão? Inclusão, segundo o dicionário Oxford é um ato ou efeito de
                    incluir(-se), ou seja, tornar algo acessível e visível para todos. E nesse caso,
                    conhecemos o Rafael Ferraz, jornalista e criador do site “Jornalista Inclusivo”, um site
                    de notícias com um foco mais inclusivo, com protagonistas PCDs que é difícil vermos de
                    fato a inclusão dessas pessoas. E tudo sobre entrevista, você confere agora: </p>

                  <p>
                    <b>Gabriella:</b> Meu nome é Gabriella, somos estudantes de jornalismo da Unitoledo Wyden,
                    estamos no 6º semestre e estamos fazendo um trabalho sobre “Acessibilidade na
                    Comunicação Jornalística.”
                  </p>
                  <p>
                    <b>Isabella:</b> Meu nome é Isabella, também sou do 6º semestre da Unitoledo Wyden, e é isso.
                  </p>
                  <p>
                    <b>Rafael:</b> Maravilha! Querem que eu me apresente? </p>
                  <p>
                    <b>Gabriella:</b> Por favor!</p>
                  <p>
                    <b>Rafael:</b> Ta bom. Meu nome é Rafael Ferraz, eu fiz faculdade de jornalismo em Itu, interior
                    de São Paulo, me formei em 2006, sempre trabalhei na área. Trabalhei com jornal
                    impresso, fiz estágio em rádio AM, trabalhei com assessoria de imprensa e de 2011 pra
                    cá, quando fiquei tretaplegico, eu passei a trabalhar com jornalismo digital. Tanto rede
                    social, quanto blogs e sites, em 2017 eu lancei o projeto Jornalista Inclusivo, onde eu
                    assinava os artigos e compartilhava publicamente publicações assinadas como Jornalista
                    Inclusivo e em 2020, começo do ano eu lancei o site jornalistainclusivo.com, já está aí
                    então com dois anos. E tenho 38 anos.
                  </p>
                  <p>
                    <b>Gabriella:</b> Então você começou a trabalhar com acessibilidade por conta do seu acidente?
                  </p>
                  <p>
                    <b>Rafael:</b> É, eu… A pessoa quando fica com alguma deficiência, diferente de quando você
                    nasce com a deficiência, você não tem informações sobre seus direitos, você não sabe
                    nada sobre acessibilidade, sobre inclusão, sobre reabilitação. Como no meu caso, foi uma
                    queda do 3º andar, eu tive várias complicações além da tetraplegia, né? Não foi só uma
                    fratura na medula que tirou meus movimentos. Tive problema no ombro, tive fratura
                    exposta de fêmur. Então foi bem complicado e eu não conhecia nada, então comecei a
                    buscar muitas informações e percebi que outras pessoas tinham as mesmas dúvidas, que eu
                    tinha. Como eu já trabalhava com comunicação, eu tive um insight e comecei a escrever
                    sobre essa busca pelos direitos. Para entender se eu tinha direito a fazer uma
                    reabilitação, se eu tinha que pagar, se a prefeitura era obrigada a pagar, quais eram as
                    leis de proteção às pessoas com deficiências. Então eu comecei a escrever em blogs como
                    convidado. Blogs que tratavam de inclusão e acessibilidade e quando foi em 2017 que veio
                    na verdade o insight de “Poxa, eu podia juntar minha condição física com a minha
                    formação acadêmica, para trabalhar em prol dessa causa.”. A causa da acessibilidade, dos
                    direitos das pessoas com deficiências, foi quando surgiu o Jornalista Inclusivo, foi
                    dessa busca, para tentar contribuir com outras pessoas.
                  </p>
                  <p>
                    <b>Gabriella:</b> E hoje em dia, quais tipo de programa o Jornalista Inclusivo oferece?
                  </p>
                  <p>
                    <b>Rafael:</b> Eu costumo dizer que Jornalista Inclusivo,o slogan do site é Acessibilidade e
                    Inclusão e notícia, a gestante busca dar protagonismo e representatividade para pessoas
                    com deficiências. As pessoas com deficiências como as outras pessoas, pagam impostos, se
                    divertem, precisam sair e precisam de acessibilidade. Então, todo tipo de notícia
                    relacionado a qualquer pessoa, inclui pessoas com deficiências ou deveria incluir as
                    pessoas com deficiências. Mas o que a gente vê na realidade é que esse público é
                    lembrado quando em datas como essa que acabou de passar, no dia 21 que é o dia de luta
                    da pessoa com deficiência, ou então no dia do surdo, o jornalista vai lá e faz uma pauta
                    sobre o dia do surdo, fala da história ou fala das conquistas. Isso não é legal, né? É a
                    mesma coisa que você só dá carinho, presente, para sua mãe nos dias das mães, você
                    lembrar do seu pai nos dias dos pais. Não, pessoas com deficiências, têm deficiência
                    todos os dias, todo dia é dia de luta para uma pessoa com deficiência, a mesma coisa
                    para pessoa preta, para pessoa que é LGBT, para pessoa que é gorda. Então, qual é a
                    ideia? Se a gente trazer o protagonismo da pessoa com deficiência, a gente vai dar uma
                    pauta, eu poderia falar sobre formas de se divertir, mas daí vou falar como uma pessoa
                    com deficiência se diverte? Ela tem deficiência física ou então ela usa cadeira de
                    rodas? Então, ela precisa de um lugar que tenha acessibilidade. Acessibilidade é lei e
                    não adianta você só ter uma rampa, uma porta mais larga. A acessibilidade envolve muitas
                    coisas, existe acessibilidade atitudinal, que é da atitude da pessoa, existe
                    acessibilidade digital, como uma pessoa surda consegue assistir um vídeo e entender o
                    que está passando no vídeo? Quando tem legenda ou quando tem libras, os mesmo são para
                    as pessoas cegas, precisa ter a descrição de imagem. O Jornalista Inclusivo é isso, a
                    gente consegue trazer informações que são protagonizadas por essas pessoas de forma
                    acessível e inclusiva. A gente precisa então tomar um certo cuidado com a nomenclatura,
                    a gente não pode falar, por exemplo, não é ideal você usar termos, como portador de
                    deficiência, ou portador de necessidades especiais. Tem um amigo meu que fala assim, “ A
                    nossa necessidade especial é amar”, cada um tem uma necessidade especial. Não, a gente
                    tem que colocar sempre pessoas antes da deficiência, então é pessoa com deficiência, eu
                    não porto uma deficiência que depois eu possa desportar, eu não escolho portar a
                    deficiência. Então uma característica, o jornalismo inclusivo é isso, você usar por
                    exemplo uma comunicação acessível.
                  </p>
                  <p>
                    <b>Gabriella:</b> A nossa conversa está muito boa, mas o tempo está curto. Quero agradecer desde
                    já o Rafael pelo tempo e pelo bate-papo. A minha colega Isabella, que nos ajudou a
                    mediar. Muito obrigada.
                  </p>
                  <p>
                    <b>Isabella:</b> Muito obrigada Rafael por essa conversa, acredito de verdade que o Jornalista
                    Inclusivo irá mudar esse cenário que vemos pessoas com deficiência e dar mais
                    visibilidade a ela. Obrigada Gabriella pela entrevista.
                  </p>
                </div>
              </div>
            </div>
          </section>

        </div>
      </main>


    </div >
  )
}

export default JornalistaInclusivo