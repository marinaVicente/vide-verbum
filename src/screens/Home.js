import BannerHome from './../assets/img/banner-home.jpg'

import AudioHome from './../assets/mp3/audio-home.mp3'


function Home() {
    return (
      <>
        <main id="content">
        <section className="container js-tabcontent">
          <div className="d-flex justify-content-center align-items-center pt-3">
            <figure className="text-center bloco-principal-destaque">
              
              <img width="80%" className="img-fluid bloco-principal-destaque-img" alt="Desenho de várias pessoas com deficiência distintas de diferentes gêneros e cores." src={BannerHome} />

                <figcaption className='mt-n3'>
                  Fonte: Banco de imagens gratuito Freepik
                </figcaption>
            </figure>
          </div>
          <div className="row justify-content-center pt-5 mb-4">
            <div className="col-12 d-flex justify-content-center">
              <audio src={AudioHome} controls />
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-md-10">
              <div id="lipsum">
                <h1 className='text-center'>Vide Verbum</h1>

                <p>
                  Todos os dias consumimos informação seja pela TV, rádio, internet,
                  smartphone. Quantas dessas vezes você já foi impedido de ler ou ouvir algo porque o site ou
                  canal não tinha um conteúdo disponível no formato que você conseguisse acessar? Se você é
                  uma pessoa com deficiência, esse é só mais um dia comum em sua vida, mas não deveria, a
                  Constituição Brasileira de 1988 garante o direito do cidadão de informar, de se informar e
                  ser informado, permitindo o livre acesso às informações públicas e privadas de relevância
                  popular.
                </p>

                <p>
                  As pessoas com deficiência também são cidadãos, eles também trabalham, compram e se divertem
                  como qualquer outra pessoa, então também deveria ter o livre acesso às informações, certo? A
                  realidade é que isso não acontece de verdade na maioria dos veículos de comunicação de massa
                  atuais no Brasil, apesar de ser um país com legislação ampla sobre acessibilidade, a maioria
                  ainda falha em disponibilizar conteúdos com janelas de libras e legendas, até mesmo na
                  internet, que deveria ser mais simples, ainda existe o impedimento.
                </p>

                <p>
                  Muitas empresas não seguem as diretrizes e boas práticas de texto na web por trás do código,
                  elas buscam ter o melhor layout para as pessoas que enxergam e se esquecem que pessoas cegas
                  usam leitores de telas para acessar seus sites, ou que pessoas surdas precisam da janela de
                  libras. Muitos acreditam que as pessoas surdas conseguem ler plenamente o texto no idioma
                  português, mas muitos foram alfabetizados em libras e assim como um estrangeiro não entende
                  bem a linguagem escrita. As barreiras vão além, por exemplo, o uso de linguagens complexas
                  impede o entendimento até mesmo de pessoas mais simples, com baixa escolaridade, idosos e
                  pessoas neurodivergentes.
                </p>

                <p>
                  Essa gama de ferramentas utilizadas pelos PCDs na hora de acessar conteúdos são chamadas
                  tecnologias assistivas. Elas são recursos simples que servem para facilitar o dia a dia do
                  PCD e que ao serem implementadas fazem toda a diferença.
                </p>

                <p>
                  Por isso, este projeto tem como objetivo a educação, conscientização e principalmente a
                  valorização do PCD como protagonista dentro da nossa sociedade, onde os recursos de
                  acessibilidade comunicacional contribuem, não apenas para o acesso dos PCDs, mas como
                  veículos de inclusão e cidadania.
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>
        
    
      </>
    )
  }

  export default Home