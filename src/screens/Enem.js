import DescricaoEnem from './../assets/mp3/audio-enem.mp3'

function Enem() {
  return (
    <div>
      <main id="content">
        <div className="container js-tabcontent">
          <section>
            <div className="row justify-content-center pt-5 mb-4">
              <div className="col-12 d-flex justify-content-center">
                <audio src={DescricaoEnem} controls />
              </div>
            </div>
            <div className="row justify-content-center mb-4">
              <div className="col-12 col-md-10 d-flex justify-content-center">
                <div className="warning-content">
                  <p>Essa matéria pertence ao site: <br />
                    <a href="https://vestibular.mundoeducacao.uol.com.br/enem/atendimento-especial-no-enem-como-funciona.htm"
                      target="_blank"
                      rel="noopener noreferrer">https://vestibular.mundoeducacao.uol.com.br/enem/atendimento-especial-no-enem-como-funciona.htm</a>
                    por Silvia Tancredi. Todos os créditos de foto e conteúdo textual pertencem aos mesmos.
                  </p>
                </div>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-12 col-md-10">
                <h1 className='text-center mb-0'>Atendimento especializado no Enem</h1>
                <p style={{ textAlign: 'center', marginBottom: '60px' }} className="not-color">Pessoas com cegueira,
                  surdos, gestantes, idosos e mais têm direito a ajuda na hora das provas</p>

                <figure className="text-center bloco-principal-destaque">
                  <img className="img-fluid bloco-principal-destaque-img"
                    src="https://static.mundoeducacao.uol.com.br/vestibular/2020/09/menina-usando-linguagem-de-sinais.jpg"
                    alt="Interpretação de linguagem de sinais é um dos recursos do atendimento especializado no Enem"
                    title="menina usando linguagem de sinais" />

                  <figcaption>
                    Interpretação de linguagem de sinais é um dos recursos do atendimento especializado no Enem
                  </figcaption>
                </figure>
                <div id="lipsum" style={{ margintop: '25px' }}>
                  <div>
                    <p>
                      A Política de Acessibilidade e Inclusão do Instituto Nacional de Pesquisas
                      Educacionais Anísio Teixeira (Inep), que organiza o Exame Nacional do Ensino Médio
                      (<strong><a
                        href="https://vestibular.mundoeducacao.uol.com.br/enem/">Enem</a></strong>),
                      garante atendimento especializado a alguns participantes nos dias de aplicação das
                      provas.&nbsp;
                    </p>
                    <p>
                      A solicitação de atendimento pode ser feita por pessoas que possuem baixa visão,
                      cegueira, deficiência física, deficiência auditiva, surdez, deficiência intelectual
                      (mental), surdocegueira, dislexia, autismo, gestante, lactante, idoso, estudante em
                      classNamee hospitalar e/ou pessoa com outra condição específica.&nbsp;
                    </p>
                    <p>
                      Mas, como funciona o atendimento especializado no Enem? São recursos de
                      acessibilidade para tornar mais adequado o espaço e/ou momento de realização da
                      prova. Sala de
                      fácil acesso, horário adicional, aparelhos auditivos, intérpretes de sinais,
                      tratamento pelo nome social e acompanhantes são alguns exemplos.&nbsp;
                    </p>

                    <h2>
                      <strong>Como pedir atendimento especializado no Enem?</strong>
                    </h2>
                    <p>
                      O atendimento especializado no Enem deve ser solicitado pelo estudante no momento da
                      inscrição. Apenas o tratamento pelo nome social, opção voltada para travestis e
                      transexuais, deve ser solicitado em um prazo específico, depois do encerramento das
                      inscrições.
                    </p>
                    <p>
                      O resultado dos pedidos é liberado cerca de 15 dias depois. Quem não foi contemplado
                      pode entrar com recurso, durante quatro dias.&nbsp;
                    </p>
                    <p>
                      Esse tipo de atendimento é um direito dos participantes e está descrito no edital do
                      Enem. Mas, em algumas situações, a pessoa precisa anexar documentos que comprovem a
                      sua condição e por que precisam do atendimento especializado. Os comprovantes devem
                      ser apresentados no momento da inscrição no Enem.
                    </p>
                    <p>
                      Alguns exemplos de documentos exigidos são certidão de nascimento, laudos médicos e
                      diagnóstico com a descrição da condição que motivou a solicitação e o código
                      correspondente à classNameificação Internacional de Doença (CID 10).
                    </p>

                    <h2><strong>Quais os tipos de atendimento especializado?</strong></h2>
                    <p>
                      Veja, a seguir, os casos de atendimento especializado no Enem mais comuns e quais
                      recursos de acessibilidade a organização do exame oferece:
                    </p>

                    <h3>Gestantes e idosos</h3>
                    <p>
                      Em ambas as situações, os inscritos contam com sala de fácil acesso e apoio para
                      pernas e pés.&nbsp;
                    </p>

                    <h3>Lactantes</h3>
                    <p>
                      Mulheres lactantes têm direito a tempo adicional de 60 minutos para ficar com o
                      bebê. No restante do tempo, ele deve ficar com um acompanhante.&nbsp;
                    </p>
                    <p>
                      As lactantes precisam apresentar certidão de nascimento do bebê ou laudo que
                      comprove a gestação com o nascimento do filho próximo à data da aplicação da prova.
                    </p>


                    <h3>Cegueira</h3>
                    <p>
                      O atendimento especializado no Enem para pessoas com cegueira contempla prova em
                      braile, ledor, transcritor, acompanhamento do cão-guia e sala de fácil acesso.&nbsp;
                    </p>
                    <p>
                      Pessoas com cegueira também podem usar materiais próprios, tais como máquina
                      Perkins, punção, reglete, assinador, tábuas de apoio, sorobã e cubaritmo – instrumentos que
                      auxiliam na escrita e em cálculos para pessoas cegas.&nbsp;
                    </p>

                    <h3>Surdocegueira</h3>
                    <p>
                      O participante surdocego tem a disposição três guias-intérpretes, prova em braile,
                      transcritor e sala de fácil acesso.
                    </p>

                    <h3>Baixa visão ou visão monocular&nbsp;</h3>
                    <p>
                      Esses estudantes têm acesso à ledor, transcritor, prova com letras e figuras
                      ampliadas e sala de fácil acesso. É disponibilizado também um programa que
                      possibilita a leitura de textos na tela do computador, por meio de voz sintetizada, que descreve tudo o
                      que aparece no monitor.
                    </p>
                    <p>
                      Pessoas com problema de visão ainda podem usar alguns materiais próprios, como
                      caneta de ponta grossa, tiposcópio, óculos especiais, lupa, telelupa e luminária.&nbsp;
                    </p>

                    <h3>Deficiência auditiva e surdez</h3>
                    <p>
                      Pessoas com deficiência auditiva ou surdez possuem tempo adicional de 120 minutos
                      por dia de prova, tradutor-intérprete de Língua Brasileira de Sinais (Libras), leitura
                      labial e videoprova em Libras.&nbsp;
                    </p>

                    <h3>Autismo, discalculia, deficit de atenção e dislexia</h3>
                    <p>
                      Alguns recursos para pessoas com essas condições são ledor, transcritor e tempo
                      adicional de 60 minutos por dia de prova.
                    </p>

                    <h3>Deficiência intelectual</h3>
                    <p>
                      Estudantes com deficiência intelectual têm à disposição ledor, transcritor e sala de
                      fácil acesso.
                    </p>

                    <h3>Deficiência física</h3>
                    <p>
                      Pessoas com deficiência física tem como atendimento especializado no Enem
                      transcritor, sala de fácil acesso e mobiliário adaptado (mesa e cadeira sem braços e
                      mesa para cadeira de rodas).
                    </p>

                    <h3>Estudante em situação de classNamee hospitalar</h3>
                    <p>
                      Participantes internados e que tenham informado essa condição no momento da inscrição
                      no Enem poderão fazer as provas no hospital desde que exista disponibilidade de
                      instalações adequadas para aplicação do exame.
                    </p>

                    <h3>Travesti/transexual</h3>
                    <p>
                      Participantes travestis e transexuais têm a opção de receber tratamento pelo nome
                      social. Os cadernos de provas são personalizados com o nome social e o participante
                      pode escolher qual banheiro usar (masculino ou feminino).
                    </p>

                    <h3>Acidentes ou imprevistos</h3>
                    <p>
                      Caso o inscrito no Enem tenham algum problema, como acidentes ou imprevistos, depois
                      do prazo de inscrições, ele precisa entrar em contato com a organização do exame por
                      meio do telefone 0800 616161, em até dez dias antes da aplicação do exame.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>

        </div>
      </main>
    </div >
  )
}

export default Enem