import FotoGabi from './../assets/img/fotos/gabriella-mendonca.jpg'
import FotoIsa from './../assets/img/fotos/isabela-hitomi.jpg'
import FotoJosi from './../assets/img/fotos/josiane-bernine.jpg'
import FotoMilena from './../assets/img/fotos/milena-dias.jpg'

import AudioQuemSomos from './../assets/mp3/audio-quem-somos.mp3'
import AudioNarration from './../assets/mp3/narration.mp3'

function QuemSomos() {
    return (
      <div>
        <main id="content">
        <div className="container js-tabcontent">
            <section>
                <div className="row justify-content-center pt-5 mb-4">
                    <div className="col-12 d-flex justify-content-center">
                        <audio src={AudioQuemSomos} controls />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-12 col-md-10">
                        <h1 className="text-center">Quem Somos</h1>
                        <p>
                            Somos alunas do 6º semestre do curso de Jornalismo do Centro Universitário Toledo Wyden -
                            UniToledo Wyden e o site Vibe Verbum é resultado da disciplina Projeto Integrador:
                            Jornalismo Multiplataforma, ministrada pela professora mestre Melissa Carolina de Moura.
                        </p>

                        <p>
                            A escolha do nosso tema buscou uma visão mais ampla sobre as dificuldades das pessoas que
                            possuem alguma deficiência encontram ao buscarem informações em diferentes meios de
                            comunicação.
                        </p>

                        <p>
                            Demos início ao aprofundamento do assunto com pesquisas e entrevistas, para entendermos mais
                            sobre esse assunto, que é tão importante, e não tem a visibilidade que deveria ter na
                            comunidade.
                        </p>

                        <p>
                            Nós buscamos pessoas que possuem voz ativa e convivem diariamente com as adversidades na
                            busca de se manter informado.
                        </p>

                        <p>
                            A nossa intenção é mostrar um pouco da acessibilidade na prática na hora de consumir
                            conteúdos e sites e veículos de comunicação, principalmente em consumo de produtos
                            jornalísticos com acessibilidade para todos.
                        </p>

                        <div className="row justify-content-center pt-5 mb-4">
                            <div className="col-12 d-flex justify-content-center">
                            <audio src={AudioNarration} controls />
                            </div>
                        </div>
                    </div>


                </div>
            </section>
            <section className="d-block">
                <h2 className="text-center" style={{marginBottom: '40px'}}>Conheça um pouquinho da gente!</h2>
                <div className="row justify-content-center">
                    <div className="col-12 col-md-5 d-flex flex-column align-items-center">
                        <div className="container-description">
                            <div className="d-flex flex-column">
                                <div className="flex-auto">
                                    <div className="container-photo">
                                        <img src={FotoGabi} alt="Foto de Gabriella segurando um microfone. Ela é branca, tem os cabelos claros e compridos, ela sorri para a foto." />
                                    </div>
                                </div>
                                <div className="flex-shrink">
                                    <div className="container-text">
                                        <span className="font-weight-bold mt-1">Gabriella Mendonça, 24 anos</span>
                                        <p>
                                            Gabriella tem interesse na área de comunicação acessível e
                                            audiodescrição. Responsável pela criação do site, pesquisas de introdução,
                                            busca por links úteis e reportagem.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-5 d-flex flex-column align-items-center mt-4 mt-md-0">
                        <div className="container-description">
                            <div className="d-flex flex-column">
                                <div className="flex-auto">
                                    <div className="container-photo">
                                        <img src={FotoIsa} alt="Foto de Isabela. Ela é asiática, faz sinal da paz com a mão e usa camisa preta." />
                                    </div>
                                </div>
                                <div className="flex-shrink">
                                    <div className="container-text">
                                        <span className="font-weight-bold mt-1">Isabela Hitomi, 22 anos</span>
                                        <p>
                                            Isabella tem interesse em jornalismo cultural. É redatora e
                                            repórter do projeto.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-5 d-flex flex-column align-items-center mt-4">
                        <div className="container-description">
                            <div className="d-flex flex-column">
                                <div className="flex-auto">
                                    <div className="container-photo">
                                        <img src={FotoJosi} alt="Foto de Josiane. Ela é branca e tem os cabelos avermelhados e cacheados, ela sorri para a foto." />
                                    </div>
                                </div>
                                <div className="flex-shrink">
                                    <div className="container-text">
                                        <span className="font-weight-bold mt-1">Josiane Bernine, 20 anos</span>
                                        <p>
                                            Josiane tem interesse pela área de redação, audiovisual e
                                            produção de conteúdo. Responsável pelas pautas, busca de fontes, pesquisas do projeto
                                            e grande reportagem.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-12 col-md-5 d-flex flex-column align-items-center mt-4">
                        <div className="container-description">
                            <div className="d-flex flex-column">
                                <div className="flex-auto">
                                    <div className="container-photo">
                                        <img src={FotoMilena} alt="Foto de Milena. Ela é branca e tem o cabelo comprido e preto, ela sorri para a foto e usa uma blusa preta de gola alta." />
                                    </div>
                                </div>
                                <div className="flex-shrink">
                                    <div className="container-text">
                                        <span className="font-weight-bold mt-1">Milena Dias, 19 anos</span>
                                        <p>
                                            Milena tem interesse na área de fotografia e audiovisual.
                                            Responsável pelas pesquisas, contato com fontes, entrevista e edição.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
        
    
      </div>
    )
  }

  export default QuemSomos