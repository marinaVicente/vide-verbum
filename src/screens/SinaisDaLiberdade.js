import AudioSinaisDaLiberdade from './../assets/mp3/audio-sinais-da-liberdade.mp3'
import ImgSinaisDaLiberdade from './../assets/img/mulher-fazendo-eu-te-amo-em-libras.jpg'
import grafico1 from './../assets/img/sinais-da-liberdade/grafico-de-nivel-de-instituicao.jpg'
import grafico2 from './../assets/img/sinais-da-liberdade/infografico-de-surdos-no-brasil.jpg'
import grafico3 from './../assets/img/sinais-da-liberdade/nivel-de-surdez.jpeg'
import entrevistaLibraria from './../assets/img/sinais-da-liberdade/entrevista-libraria.png'
// import videoUrl from './../assets/videos/entrevista-libraria.mp4'
// import video2 from './../assets/img/videos/entrevista-gigi.mp4'

function SinaisDaLiberdade() {
  return (
    <div>
      <main id="content">
        <div className="container js-tabcontent">

          <section>
            <div className="row justify-content-center pt-5 mb-4">
              <div className="col-12 col-md-10 d-flex justify-content-center">
                <audio src={AudioSinaisDaLiberdade} controls />
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-12 col-md-10">
                <h1 className="text-center">Sinais da Liberdade</h1>
                <div className="foto-apresentacao">
                  <figure className="text-center bloco-principal-destaque">
                    <img className="img-fluid bloco-principal-destaque-img"
                      src={ImgSinaisDaLiberdade}
                      alt="Mulher fazendo o sinal de eu te amo em libras."
                    />

                    <figcaption>
                      Fonte: Banco de imagens gratuito Freepik
                    </figcaption>
                  </figure>
                </div>
                <div id="lipsum">
                  <p>
                    Descubra como as Libras (Linguagem Brasileiras de Sinais) podem ser uma ferramenta de
                    liberdade e autonomia para a comunidade surda e como a falta de acessibilidade dificulta
                    a vida destes mais de 10 milhões de brasileiros.
                  </p>

                  <p>
                    O mês de setembro foi marcado pela luta pela inclusão da pessoa com deficiência. E no
                    dia a dia das pessoas surdas, esta é uma luta diária. No dia 26 de setembro é celebrado o dia
                    nacional do surdo e dia nacional da linguagem de sinais.
                  </p>

                  <p>
                    Um ouvinte cresce ciente das suas próprias liberdades e conforme a idade, pode ter
                    autonomia sobre todas as coisas em sua vida. Ele pode ir ao médico sozinho, ao mercado, ir ao
                    banco resolver suas pendências e no final do dia, assistir ao noticiário na TV, de qualquer
                    lugar de sua casa, para saber as últimas notícias do dia. Possivelmente este ouvinte adulto e
                    independente, não sofre com muitos problemas de comunicação para fazer estas coisas
                    básicas em seu cotidiano.
                  </p>

                  <p>
                    Para um adulto surdo a realidade é outra. Atividades cotidianas necessárias que poderiam
                    ser feitas sem maiores problemas, podem ser de grande dificuldade para um surdo, quando os
                    ambientes não disponibilizam acessibilidade na comunicação. Uma ida ao psicólogo, ao
                    médico, ao banco e ao mercado podem exigir a presença de um acompanhante que venha a traduzir
                    todos os itens mais importantes. Retirando desta forma, toda autonomia que um surdo poderia
                    ter ao fazer coisas básicas de seu cotidiano.
                  </p>

                  <p>
                    Isso ocorre pois boa parte dos ambientes e mecanismos de comunicação não disponibilizam
                    as informações de forma acessível.
                  </p>

                  <p>
                    O entrave na comunicação entre surdos e profissionais de saúde pode gerar muitos
                    transtornos, como o não entendimento do problema do paciente, não estabelecimento de
                    relação e confiança por parte do surdo e até mesmo o entendimento de um tratamento de saúde
                    ineficaz
                  </p>

                  <p>
                    Mas, como contornar um problema que atinge 5% da população do país, mas que ainda não é
                    tratado como uma prioridade?
                  </p>

                  <p>
                    Há alguns anos foram criadas leis para garantir esse acesso da população surda à saúde
                    no Brasil. Dentre elas, o decreto nº 5.626 de 22/12/2005, Capítulo VII, Art.25, que dispõe
                    sobre a garantia e direito da pessoa surda em ter acesso a Língua Brasileira de Sinais
                    (Libras) nos serviços de saúde. Para isso, 5% dos profissionais das unidades do serviço
                    público, devem ser capacitados para uso e interpretação da linguagem de sinais.
                  </p>

                  <p>
                    Desde 2016, a LBI (Lei Brasileira de Inclusão) determina que sites públicos e privados
                    devem ser acessíveis para os mais de 45 milhões de brasileiros com algum tipo de deficiência,
                    de acordo com dados do IBGE (Instituto Brasileiro de Geografia e Estatística) publicados em
                    2019.
                  </p>

                  <p>
                    No entanto, menos de 1% dos portais seguem a regra. A análise é da terceira edição de
                    uma pesquisa da BigDataCorp, realizada em parceria com o Movimento Web para Todos.
                  </p>

                  <p>
                    E por que as empresas não se atentam a este problema?
                  </p>

                  <p>
                    A acessibilidade na comunicação deve oferecer recursos e bens culturais que promovem
                    independência e autonomia aos indivíduos que necessitam de serviços específicos para
                    acessar os conteúdos.
                  </p>

                  <p>
                    A linguagem é parte integrante no desenvolvimento do ser humano. A falta dela tem graves
                    consequências para o indivíduo no que se refere ao seu desenvolvimento emocional, social
                    e intelectual. A comunicação é um processo de interação no qual compartilham-se mensagens,
                    ideias, emoções e sentimentos, podendo influenciar ou não outras pessoas. No entanto, a
                    comunicação nem sempre ocorre de forma clara, uma vez que há várias crianças, jovens e
                    adultos com deficiência na audição e consequentemente na comunicação.
                    Segundo o último censo realizado pelo Instituto Brasileiro de Geografia e Estatística
                    (IBGE) em 2020, mais de 10 milhões de pessoas têm algum problema relacionado à surdez, ou seja,
                    5% da população. Entre elas, 2,7 milhões não ouvem nada.
                  </p>

                  <p>
                    De acordo com a Organização Mundial da Saúde (OMS), a estimativa é de que 900 milhões de
                    indivíduos no mundo todo podem desenvolver surdez até 2050.
                  </p>

                  <img className="mt-4" src={grafico1} alt="Gráfico de pizza em que se mostra o grau de instituição de surdos no Brasil" width="100%" />
                  <img className="mt-4 mb-4" src={grafico2} alt="Infográfico mostrando a quantidade de surdos no Brasil" width="100%" />
                  <img src={grafico3} alt="Imagem mostrando a comparação entre pessoas com problemas relacionados a surdez e pessoas que são totalmente surdas" width="100%" />

                  <h2>Métodos Acessíveis</h2>
                  <p>
                    Um dos mecanismos que mais vem sendo utilizados com a premissa de ser mais inclusivo,
                    são as legendas. Porém, apenas as legendas não resolvem a maior parte do problema, pois grande
                    parte dos surdos não são alfabetizados com a língua portuguesa como conhecemos.
                  </p>

                  <p>
                    Segundo estudo feito pelo Instituto Locomotiva e a Semana da Acessibilidade Surda em
                    2019, cerca de 7% dos surdos brasileiros têm ensino superior completo, 15% frequentaram a
                    escola até o ensino médio, 46% até o fundamental, enquanto 32% não têm um grau de instrução.
                  </p>

                  <p>
                    Algumas pessoas nascem com problemas auditivos, e não conseguem ouvir o que é dito pelos
                    outros. Devido a deficiência auditiva a fala fica prejudicada e não são raros os casos
                    em que ela não é desenvolvida. As pessoas que apresentam essa deficiência geralmente se
                    comunicam através de gestos, numa linguagem própria, feita através de sinais. Essa
                    linguagem recebe a nomenclatura de Língua Brasileira de Sinais, mais conhecida como LIBRAS.
                  </p>
                  <h3>Língua de Sinais</h3>
                  <p>
                    Os surdos criaram uma Língua de Sinais, e através dela podem comunicar-se tão bem quanto
                    os ouvintes, pois ela permite a melhor integração entre pessoas surdas e/ou. A Língua de
                    Sinais é uma língua visoespacial e se apresenta em uma modalidade diferente da língua oral, uma
                    vez que utiliza a visão e o espaço, e não o canal oral- auditivo, ou seja, a fala. A Língua
                    de Sinais faz uso de movimentos e expressões corporais e faciais que são percebidos pela
                    visão.
                  </p>

                  <h3>Libras</h3>
                  <p>
                    A Língua Brasileira de Sinais - LIBRAS - é a língua materna dos surdos brasileiros. Foi
                    assim denominada durante a Assembleia convocada pela FENEIS, Federação Nacional de
                    Educação e Integração dos Surdos, em outubro de 1993.
                    Todas as línguas, sejam elas orais ou sinalizadas, são estruturadas a partir de uma
                    unidade mínima que formam unidades mais complexas. Porém, as pessoas geralmente acreditam que a
                    Libras é uma representação da língua portuguesa através dos gestos, devido a modalidade
                    sinalizada. Mas, ela não é derivada do português e também não é uma língua simplificada,
                    pois contém estruturas e processos que não se encontra no português. A Libras é uma
                    língua completa e possui uma gramática própria e única.
                  </p>

                  <h2>A importância da comunicação</h2>
                  <p>
                    O que mais angustia os pais de pessoas surdas não é a surdez em si, mas o obstáculo na
                    comunicação que ela proporciona. Muitos pais não estabelecem a Língua de Sinais na
                    comunicação com seus filhos, porque desconhecem a importância dela para o
                    desenvolvimento psíquico-social e ainda como uma forma de aquisição dos conhecimentos
                    das pessoas surdas. Há por parte deles a ilusão de que seus filhos possam ouvir ou tornarem-se
                    semelhantes aos ouvintes. Para tanto, buscam atendimentos, tratamentos clínicos e educação oralista na
                    tentativa de oferecer aos filhos surdos, a oportunidade de constituírem-se como sujeitos
                    e cidadãos através da linguagem oral. Porém, a utilização da Linguagem Brasileira de Sinais é uma forma de garantir a
                    preservação da identidade das pessoas e comunidades surdas. Além disso, contribui para a
                    valorização e reconhecimento da cultura surda que, por tanto tempo, foi o alvo da hegemonia da
                    cultura ouvinte.
                    A comunicação através da Libras, propicia uma melhor compreensão entre surdos e
                    ouvintes, uma vez que já está previsto em lei a presença de intérpretes de Libras em
                    diferentes instituições públicas , como escolas, universidades, congressos, seminários, programas
                    de televisão entre outros. Além disso, a utilização das libras facilita a comunicação
                    entre os surdos, que passam a se compreender como uma comunidade que tem características
                    comuns e que devem ser reconhecidas como tal, praticando assim, a verdadeira inclusão social.
                    A pessoa surda, através da Língua de Sinais, pode desenvolver integralmente todas as
                    suas possibilidades cognitivas, afetivas e emocionais, permitindo sua inclusão e
                    integração na sociedade. Por isso, é imprescindível que os pais de crianças surdas estabeleçam
                    contato com a Língua de Sinais o mais cedo possível, aceitando a surdez de seus filhos como
                    diferença e a Libras como uma modalidade de comunicação.
                    O atraso na aceitação deste fato pode acarretar prejuízos no desenvolvimento cognitivo,
                    emocional e da comunicação da criança surda, uma vez que a utilização da Libras pelos
                    surdos possibilita o entendimento podendo ainda facilitar o atendimento de suas necessidades,
                    seus anseios e suas expectativas. É por meio dessa língua que o surdo fará a interação
                    na sociedade, construir sua identidade e exercer sua cidadania, sendo esta, a forma mais
                    expressiva de inclusão.
                  </p>
                </div>
              </div>
            </div>
          </section >
          <section className="d-block">
            <div className="row justify-content-center">
              <div className="col-12 col-md-10">
                <h2 className="text-center">O Libraria News</h2>

                <p>
                  O Libraria News é uma plataforma que disponibiliza as notícias da atualidade e informações
                  que estão em alta em libras. O projeto foi uma iniciativa do Douglas Reis, com o intuito de tornar a
                  comunicação mais acessível para a comunidade surda.
                </p>

                <p>
                  Hoje, o Douglas é responsável pela produção, análises tradutórias, escolha de intérpretes,
                  quais notícias serão divulgadas e edição. O Luciano Esportes, seu sócio, é responsável pela área
                  comercial e administrativa.
                </p>

                <img className="mb-4 w-100" src={entrevistaLibraria} alt="Imagem de uma reunião com os diretores da empresa Libraria"/> 

                <h3>Como começou?</h3>

                <p>
                  O Libraria News começou a partir de uma situação em que o Douglas, que já atuava como
                  intérprete em 2011 no Estado do Mato Grosso do Sul, teve a ideia de traduzir os materiais da
                  universidade, que não eram acessíveis para surdos.
                </p>

                <p>
                  A partir de então, ele seguiu com um sonho de produzir materiais acessíveis e em 2014 criou
                  com mais quatro amigos um canal de entretenimento com quadros sobre maquiagens, viagens,
                  curiosidades, tecnologia e outros. O Libraria News nasceu em 26 de setembro de 2014, nas plataformas do
                  youtube e no google, quando foi publicado o primeiro vídeo. Em fevereiro de 2015, começaram as
                  notícias em libras. O objetivo era fazer testes, para que a comunidade surda desse feedbacks e o canal
                  se tornasse mais completo.
                </p>

                <p>
                  De 2018 em diante a equipe do Libraria começou a investir mais na produção, para que o
                  trabalho se tornasse mais profissional e o site chegasse a nível nacional. Até hoje foram produzidos
                  quase dois mil vídeos de notícias, com seis vídeos divulgados semanalmente, todos acessíveis em libras.
                </p>

                <p>
                  Hoje a Libraria conta com mais de 100 colaboradores por todo o Brasil, dentre eles,
                  intérpretes que atuam em eventos presenciais e onlines, equipe de gravação, editores, tradutores e técnicos
                  de produção. Só no último ano a Libraria News cresceu 3.000% e faculdades já estão utilizando
                  os conteúdos para ajudar nos cursos.
                </p>

                <p>
                  Uma das principais motivações da Libraria para produzir as notícias com excelência, é
                  primeiro ver o que a comunidade surda entende como língua de sinais, como eles gostariam de receber as
                  notícias e a partir das opiniões deles, melhorar as traduções. Para atingir o maior nível de compreensão
                  possível para a comunidade, o Libraria ficou mais de um ano em beta para a comunidade definir como o
                  portal ficaria mais claro e acessível.
                </p>

                <p>
                  Uma dessas personificações que foram ajustadas, é que quando a notícia vai para o youtube,
                  elas vão com o texto de capa, pois muitos intérpretes ouvintes gostam de acompanhar as traduções do
                  Libraria News, mas quando as notícias vão para o site, elas estão sem o texto de capa, porque para o
                  surdo interessa mais um termo mais simples e a capa apenas a imagem, pois eles preferem o visual
                  do que o português.
                </p>

                <p>
                  Alguns dos maiores diferenciais do Libraria, foi que durante a guerra da Ucrânia, a empresa
                  se preocupou em divulgar diariamente o que estava acontecendo, e foi a primeira vez no Brasil o
                  surdo teve acesso a uma cobertura completa jornalística.
                </p>

                <p>
                  Outro ponto a se destacar, é que a fornece diversas ferramentas para empresas tornarem o
                  ambiente de atendimento mais acessível para a comunidade surda. A maioria das empresas não acessibilizam
                  seu conteúdo por falta de informação, que é a primeira barreira das objeções normais de uma
                  venda do Libraria News, segundo Luciano, responsável pela área comercial.
                </p>

                <p>
                  As empresas se enganam acreditando que os surdos entendem o português e que apenas o uso de
                  textos e legendas, já torna o ambiente acessível, mas a grande maioria dos surdos não entende a
                  língua portuguesa.
                </p>

                <h2>Português sinalizado x libras pura</h2>

                <p>
                  Um dos grandes problemas que eles encontram com os intérpretes é o português sinalizado, que
                  busca seguir um sinal da língua portuguesa e atrapalha que bons intérpretes façam a tradução e
                  entendam esta diferença de linguagem. Muitas pessoas aprendem libras na faculdade que estão muito
                  mais próximas do português sinalizado, não é a libras que o surdo usa no dia a dia. Estes surdos
                  têm uma maneira de produzir suas ideias seguindo a estrutura gramatical da libras.
                </p>

                <p>
                  O português sinalizado dificulta que o surdo entenda as traduções, pois existem surdos que
                  fizeram mestrado, doutorado e tiveram muito acesso ao português, mas a grande maioria dos surdos que
                  trabalham não vivem esta realidade e não tem esta mesma facilidade. Esses surdos desenvolvem
                  uma maneira de fazer libras que a Libraria alcunhou de “libras pura”, que seria a libra dentro
                  da comunidade surda, mais espontânea e muito diferente do português sinalizado.
                </p>

                <p>
                  Por este motivo, a Libraria conta com surdos na equipe de trabalho, produzindo as
                  interpretações e corrigindo as traduções dos intérpretes, para que as notícias em libras sejam de fato,
                  acessíveis para todos dentro e fora da empresa.
                </p>

                <h2>Como produzir notícias com excelência?</h2>

                <p>
                  Primeiro precisaram entender a cultura surda, qual é a personalidade e o sentimento do
                  surdo. O Libraria News entendeu que levar informação, era mais que pegar notícias, transformar em
                  libras e levar para o surdo. Era usar o sinal corretamente, se preocupar com o tipo de comunicação,
                  para ser acessível ao surdo que está aprendendo libras e ao surdo que já conhece a anos. Uma das
                  preocupações principais deles era fazer uma libras simples, para que qualquer um pudesse entender.
                </p>

                <h2>Acessibilidade na comunicação</h2>

                <p>
                  Luciano, o sócio da empresa, acredita que as pessoas precisavam ter um olhar diferente na
                  questão da comunicação. A comunicação é visual, escrita, verbal e não-verbal . Quando você não a torna
                  acessível para um surdo, impede que ele tenha acesso a informações de coisas simples, como
                  ir ao mercado e não pode entender as fichas técnicas dos produtos, as receitas das embalagens.
                  Comprar medicamentos e não ter acesso às bulas, correndo o risco de ter uma reação e não poder
                  verificar se deve suspender a medicação.
                </p>

                <p>
                  Esta falta de acessibilidade exclui mais de seis milhões de pessoas. Ela não pode falar com
                  o dentista, com a psicóloga, com o posto de saúde. Se sofre uma emergência, ele não consegue
                  comunicar o que está sentindo para um samu ou para uma ambulância.
                </p>

                <p>
                  Douglas pontua que a importância da comunicação se faz clara na autonomia, dignidade e
                  respeito que são tidas com a comunidade surda. Ele acredita que a comunidade surda fica invisível, pois
                  os surdos não têm os problemas que os ouvintes percebem imediatamente, como a cegueira, ou uma
                  deficiência motora. A barreira da comunicação é simples de resolver, mas como ela não é vista pelos
                  ouvintes, parece que ela não existe para as empresas.
                </p>

                <p>
                  Profissionalmente, muitos surdos que trabalham não têm a oportunidade de atuar em setores de
                  comunicação, nem assistir aos cursos internos da empresa por não estar acessível em libras.
                  Eles perdem oportunidades de ascensão na carreira por falta de acessibilidade e as opções
                  profissionais disponíveis se tornam as mais pesadas e manuais, como almoxarifado e outros.
                </p>

                <p>
                  Para contar um pouco da sua experiência pessoal com a acessibilidade na comunicação,
                  entramos em contato também com a Gisleile Firmino, conhecida como Gigi, que é surda e atua como
                  tradutora no Libraria News
                </p>

                <video width="100%" controls className="mb-4">
                  <source src={require('./../assets/videos/entrevista-gigi.mp4')} type="video/mp4"/>
                </video>
                {/* <video width="100%" controls className="mb-4">
                  <source src={require('./../assets/videos/entrevista-libraria.mp4')} type="video/mp4"/>
                </video> */}

                <p>
                  A entrevista foi realizada pela aluna Milena Dias com a mediação da intérprete Flávia
                  Santos, em 26 de setembro, no dia nacional do surdo e dia internacional da linguagem de sinal.
                </p>

                <p>
                  A Gigi pontua que acompanha notícias principalmente pelo youtube, onde ela encontra mais
                  facilmente informações acessíveis em libras e também pela televisão, mas que raramente os canais
                  disponibilizam intérpretes. Onde ela mais sente falta de acessibilidade na comunicação é no setor da saúde,
                  onde ela raramente encontra informações voltadas para o setor de forma acessível.
                </p>
                <p>
                  Para Gisleile o que falta para os meios de comunicação se tornarem mais acessíveis seria não
                  apenas as legendas, que algumas plataformas já disponibilizam, mas o uso de intérpretes nas
                  notícias e matérias para que ela e diversos outros surdos compreendam com mais autonomia e clareza.
                </p>

                <p>
                  Um dos maiores problemas que ela destaca pela falta de acessibilidade é na comunicação das
                  empresas, seja no e-mail marketing que ela recebia todos escritos em português e sem nenhuma tradução
                  em libras ou nos atendimentos em lugares públicos, como bancos e setores de saúde, em que ela
                  sempre precisa de assistência de alguém para traduzir as informações e muitas vezes não encontra.
                </p>

                <p>
                  Para o dia nacional do surdo, Gigi destaca que a importância da conscientização da data está
                  em tornar a pauta pública para que a comunicação das pessoas surdas se torne mais clara, com
                  mais informações, e então a comunidade surda tenha o poder de se desenvolver e ter mais
                  responsabilidades.
                </p>

                <p>
                  As libras são, para a comunidade surda, como os sinais da liberdade. A autonomia e a
                  evolução de mais de 10 milhões de brasileiros, depende de empresas, pessoas como eu e você, entender que
                  a comunicação como temos hoje pode excluir toda uma comunidade que não conta com acesso para
                  as coisas mais básicas do cotidiano. A linguagem de sinais garante a preservação da identidade das
                  pessoas e da comunidade surda, além de contribuir para a valorização e reconhecimento destes.
                </p>

                <p>
                  É importante que nos conscientizemos, como formadores de opiniões, profissionais da
                  comunicação e trabalhadores do cotidiano, que a nossa parte deve ser feita, para uma comunicação
                  universal, acessível e clara, que inclua todos os brasileiros e permita que a linguagem, as notícias e
                  as informações, que são primordiais para formar a sociedade brasileira, sejam para todos e
                  principalmente, que não exclua a realidade de ninguém.
                </p>
              </div>
            </div>
          </section>
        </div >
      </main >


    </div >
  )
}

export default SinaisDaLiberdade