import DescricaoLinks from './../assets/mp3/links-uteis.mp3'
import accessmonitor from './../assets/img/links-uteis/acess-monitor.png'
import LowVision from './../assets/img/links-uteis/low-vision.png'
import insight from './../assets/img/links-uteis/accessibility.png'
import nvda from './../assets/img/links-uteis/nvda.png'
import equal from './../assets/img/links-uteis/equal.png'
import ttsreader from './../assets/img/links-uteis/ttsreader.png'
import wideo from './../assets/img/links-uteis/wideo.png'
import govbr from './../assets/img/links-uteis/govbr.png'
import w3c from './../assets/img/links-uteis/w3c.png'

function LinksUteis() {
  return (
    <div>
      <main id="content">
        <div className="container js-tabcontent">
          <section>
            <div className="row justify-content-center pt-5 mb-4">
              <div className="col-12 d-flex justify-content-center">
                <audio src={DescricaoLinks} controls />
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-10">
                <h1 style={{ textAlign: 'center', marginBottom: '25px' }}>Links úteis</h1>
                <div id="lipsum">
                  <p>
                    Agora que você entendeu melhor sobre a acessibilidade comunicacional que tal aplica-la
                    no seu dia a dia? Para isso separamos alguns links que podem te guiar nessa jornada a
                    ajudar a informação a se tornar mais acessivel para todos.
                  </p>
                  <div>
                    <h2 className="font-weight-bold">
                      <a href="https://accessmonitor.acessibilidade.gov.pt/" target="_blank" rel="noopener noreferrer">
                        Access Monitor
                      </a>
                    </h2>
                    <p>
                      Essa ferramenta criada pelo governo de portugal avalia a acessibilidade do seu site e
                      aponta o que está certo, o que está errado e o que pode melhorar. Você consegue inserir pelo
                      link ou pelo próprio código html do site.
                    </p>
                    <img className="links-uteis-img" src={accessmonitor} alt="Captura de tela da ferramenta Access Monitor sendo usada no site Vide verbum" width="55%" />
                  </div>
                  <div className="mt-4">
                    <h2 className="font-weight-bold">
                      <a href="http://lowvision.support/" target="_blank" rel="noopener noreferrer">
                        Low Vision
                      </a>
                    </h2>
                    <p>
                      É um site simples onde você coloca o link do website que deseja avaliar e ele te
                      disponibiliza diferentes condições de visão, principalmente as do daltonismo e assim
                      você tem noção quais cores do site não estão acessíveis para essas pessoas.
                    </p>
                    <img className="links-uteis-img" src={LowVision} alt="Captura de tela da ferramenta Low Vision sendo usada no site Vide verbum" width="55%" />
                  </div>
                  <div className="mt-4">
                    <h2 className="font-weight-bold">
                      <a href="https://accessibilityinsights.io/docs/web/overview/" target="_blank" rel="noopener noreferrer">
                        Accessibility Insights
                      </a>
                    </h2>
                    <p>
                      Aqui você encontra uma extensão para o seu navegador de internet ou para usar no
                      computador e checar seu site e código como um todo, passo a passo, manualmente ou automaticamente.
                      Ele mostra o caminho que um leitor de tela percorre e te ajuda a encontrar problemas de
                      acessibilidade de acordo com as diretrizes internacionais da W3C.
                    </p>
                    <img className="links-uteis-img" src={insight} alt="Captura de tela da ferramenta Accessibility Insights sendo usada no site Vide verbum" width="55%" />
                  </div>
                  <div className="mt-4">
                    <h2 className="font-weight-bold">
                      <a href="https://www.nvaccess.org/download/" target="_blank" rel="noopener noreferrer">
                        NVDA
                      </a>
                    </h2>
                    <p>
                      É um dos leitores de tela mais usados no Brasil e é gratuito para download.
                    </p>
                    <img className="links-uteis-img" src={nvda} alt="Captura de tela da ferramenta NVDA sendo usada no site Vide Verbum" width="55%" />
                  </div>
                  <div className="mt-4">
                    <h2>
                      <a href="https://equalweb.com.br/" target="_blank" rel="noopener noreferrer">
                        EqualWeb
                      </a>
                    </h2>

                    <p>Site especializado que vende recursos de acessibilidade digital para websites de empresas de qualquer tamanho</p>
                    <img className="links-uteis-img" src={equal} alt="Captura de tela do site EqualWeb mostrando os recursos de acessibilidade que eles disponibilizam/vendem" width="55%" />
                  </div>

                  <h2 className="mt-4 font-weight-bold">Conversores de texto para aúdio</h2>
                  <div>
                    <p>
                      Aqui você encontra duas ferramentas de conversão de texto para áudio,
                      o <a href="https://ttsreader.com/" target="_blank" rel="noopener noreferrer">Tts Reader</a> e o
                      <a href="https://wideo.co/text-to-speech/" target="_blank" rel="noopener noreferrer">Wideo</a>,
                      as versões gratuitas estão disponíveis para testes e tem limite de caracteres por dia.
                    </p>
                    <a className="d-block mb-4" href="https://ttsreader.com/">Tts Reader</a>
                    <img className="links-uteis-img" src={ttsreader} alt="Captura de tela do site Tts Reader mostrando os recursos de acessibilidade que eles disponibilizam/vendem" width="55%" />
                    <a className="d-block mb-4" href="https://wideo.co/text-to-speech/" target="_blank" rel="noopener noreferrer">Wideo</a>
                    <img className="links-uteis-img" src={wideo} alt="Captura de tela do site Wideo mostrando os recursos de acessibilidade que eles disponibilizam/vendem" width="55%" />
                  </div>
                  <h2 className="mt-4 font-weight-bold">Educacionais</h2>
                  <div>
                    <p>
                      Se você quer saber mais sobre a acessibilidade web e como ela contribui para a
                      comunicação acessível você tem de conhecer a <a href="https://mwpt.com.br/" target="_blank" rel="noopener noreferrer">MWPT</a>, esse portal brasileiro que
                      promove a inclusão das pessoas com deficiência na web conta com vários conteúdos
                      educativos sobre acessibilidade.
                    </p>
                    <img className="links-uteis-img" src="./assets/img/links-uteis/mwpt.png" alt="Captura de tela do site MWPT (Movimento Web Para Todos)" width="55%" />
                  </div>
                  <div>
                    <h2 className="mt-4 font-weight-bold">
                      <a href="https://www.gov.br/governodigital/pt-br/acessibilidade-digital" target="_blank" rel="noopener noreferrer">GOV.BR</a>
                    </h2>
                    <p>
                      O site do governo brasileiro <a href="https://www.gov.br/governodigital/pt-br/acessibilidade-digital" target="_blank" rel="noopener noreferrer">GOV.BR</a> possui uma página apenas sobre as diretrizes
                      brasileiras de acessibilidade de conteúdos e páginas governamentais e traz exemplos e ferramentas
                      que podem ser usadas por todos
                    </p>
                    <img className="links-uteis-img" src={govbr} alt="Captura de tela do site gov.br mostrando os recursos de acessibilidade que eles disponibilizam" width="55%" />
                  </div>

                  <div>
                    <h2 className="mt-4 font-weight-bold">
                      <a href="https://www.w3.org/TR/WCAG21/" target="_blank" rel="noopener noreferrer">W3C</a>
                    </h2>
                    <p>
                      Se você quiser se aprofundar ainda mais você pode acessar o site da
                      <a href="https://www.w3.org/TR/WCAG21/" target="_blank" rel="noopener noreferrer">W3C</a> (consórcio internacional que desenvolve padrões para a Web em todo o mundo) e conferir a
                      <a href="https://www.w3.org/TR/WCAG21/" target="_blank" rel="noopener noreferrer">WCAG</a>(Diretrizes de Acessibilidade para o Conteúdo da Web)
                    </p>
                    <img className="links-uteis-img" src={w3c} alt="Captura de tela do site W3C mostrando as diretrizes de acessibilidade" width="55%" />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </main>
    </div >
  )
}

export default LinksUteis