export const lightTheme = {
  body: "#FFFFFF",
  text: "rgb(33, 37, 41)",
  header: "rgb(9, 9, 121)",
  headingTags: "rgb(9, 9, 121)",
  topBar: "#D3D3D3",
  link: '#FFFFFF',
  linkGeral: '#007bff',
  teste: 'none',
  svg: "rgb(9, 9, 121)"
};

export const darkTheme = {
  body: "#121212",
  header: "#121212",
  topBar: "#121212",
  text: "#FFFFFF",
  headingTags: "#FFFFFF",
  link: "#FFF333",
  linkGeral: "#FFF333",
  svg: "#FFF333",
  teste: 'underline'
};



